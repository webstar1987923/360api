'use strict';

var express = require('express');
var fs = require("fs");

var contents = fs.readFileSync('discovery.json');
var services = fs.readFileSync('services.json');

var jsonContent = JSON.parse(contents);
var servicesContent = JSON.parse(services);

var app = express();

app.get('/', function(req, res) {
  res.json(jsonContent);
});

app.get('/services', function(req, res) {
  res.json(servicesContent);
});

// Constants
var PORT = 8888;

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);