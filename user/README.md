# Node MongoDB Restify basic CRUD
1. Install Node.js, MongoDB. The app will create and use a database named "users" if not changed
3. Install with `npm install`
4. Start server: `node index.js`
5. Try it out:

    `$ curl --form "name=Marius&weight=6.3&age=12" localhost:8080/customer`

    `$ curl --form "name=Brack&weight=8.2&age=9" localhost:8080/advisor`

    `$ curl --form "name=shopey&weight=22.3&age=11" localhost:8080/supervisor`

    `$ curl localhost:8080/companysuperuser`

