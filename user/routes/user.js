'use strict';

const User = require('../models/user');

module.exports = server => {
    
    /**
     * Handle GET requests on users
     * Return a list of all the users to the client
     */
    server.get('/users', (req, res, next) => {
        if(req.query.email){
            User.findOne({email:req.query.email}, (err, user) => {
                    res.send(user);
                    next();
            });
        }else{
            User.find((err, users) => {
                res.send(users);
                next();
            });
        }
    });

    /**
     * Handle GET requests on specific user
     * Return a description of the user to the client
     */
    server.get('/users/:id', (req, res, next) => {
        User.findOne({ _id:req.params.id }, (err, user) => {
            res.send(user);
            next();
        });
    });


    
    /**
     * Handle POST requests on users
     * Create a new user in database
     */
    server.post('/users', (req, res, next) => {
        let user = new User({
            first_name: req.body.firstname,
            last_name: req.body.lastname,
            email: req.body.email,
            roles: req.body.roles,
            groups: req.body.groups
        });
        for (let prop in req.body)
            user[prop] = req.body[prop];
        user.save().then(user => {
            res.send(user);
            next();
        });
    });
    
    /**
     * Handle PUT requests on specific user
     * Update an existing user in database
     */
    server.put('/users/:id', (req, res, next) => {
        User.findOne({ _id:req.params.id }, (err, user) => {
            for (let prop in req.body)
                user[prop] = req.body[prop];
            user.save().then(user => {
                res.send(user);
                next();
            });
        });
    });
    
    /**
     * Handle DELETE requests on specific user
     * Delete an existing user in database
     */
    server.del('/users/:id', (req, res, next) => {
        User.remove({ _id: req.params.id }, err => {
            res.send({ok: 'ok'});
            next();
        });
    });
    
}