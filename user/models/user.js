'use strict';

let mongoose = require('mongoose');

/* The schema describing the collection in mongo database */
let schema = new mongoose.Schema({ 
    uid: String, 
    first_name: String, 
	last_name: String,
	email: String,
	picture: String,
	phone: String,
	address: String,
	locale: String,
	roles: Array,
	groups: Array
});

/* We export the model to use it in user.js routes file */
module.exports = mongoose.model('User', schema);