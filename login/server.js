var express 	= require('express');
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var jwt         = require('jsonwebtoken');
var config      = require('./config');
var request     = require('request-promise');
var q           = require('q');

var app         = express();
var port        = process.env.PORT || 8110;
var fs = require("fs");
var login_post_options = fs.readFileSync('login-post.json');

var discoveryData = "";
var discoveryServiceData = "";

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// =================================================================
function initDiscovery(){
    request.get(config.discovery).then( function(data){
        discoveryData = JSON.parse(data)
    });
    request.get(config.discovery_services).then( function(data){
        discoveryServiceData = JSON.parse(data)
    });
}

function getDiscoveryLink(relation, data){
	var links = data["links"];
	var url = '';

	links.forEach(function (key, value){
		if(key.rel==relation){
			url = key.url_proxy;
			return;
		}
	});

	return url;
}

app.options('/', function(req, res) {
    res.json({"Allow": "POST,OPTIONS", "Content-Type": "application/json"});
});

app.options('/login', function(req, res) {
    res.json(JSON.parse(login_post_options));
});

app.post('/login', function(req, res) {
    var sessionPromise = createSession(req);
    var userPromise = sessionPromise.then(getUser.bind(null,req));

    q.all([sessionPromise, userPromise]).then(prepareResponse.bind(null, req, res)).
    catch(function (error) {
         console.log(error.toString('utf8'));
         return res.json({error:[{"message":error.message}]});
    }).
    done();
});

function createSession(req){
    var options = {
        method: 'POST',
        uri: getDiscoveryLink("session-creation", discoveryData),
        body: { email: req.body.email, password:req.body.password },
        headers: { "Content-Type": "application/json" },
        json: true
    };

    return request(options).then(function(data){
        if (data.success == false){
            throw new Error(data.message)
        }
        console.log("session created!");
        return data;
    });
}

function getUser(req, data){
    var options = {
        method: 'GET',
        uri: getDiscoveryLink("users", discoveryServiceData) + "?email=" + req.body.email,
        headers: {  "Content-Type": "application/json", "x-access-token": data.token},
        json: true
    };

    return request(options).
    then(function (body) {
        console.log("user fetched");
        return body;
    });
}

function prepareResponse(req, res, data){
    var user_id = data[1]._id;
    var user = data[1];
    var token = data[0].token;

    var options = {
        method: 'GET',
        uri: getDiscoveryLink("companies", discoveryServiceData) + "?short_id=" + user.groups[0],
        headers: {  "Content-Type": "application/json", "x-access-token": token},
        json: true
    };

    return request(options).
        then(function (body) {
            return res.json({
                success: true,
                token: token,
                links:[
                    {
                      "user": getDiscoveryLink("users", discoveryServiceData) + "/" + user_id,
                      "company": getDiscoveryLink("companies", discoveryServiceData) + "/" + body._id
                    }
                ]
            });
        });
}

// =================================================================

app.listen(port, function() {
  initDiscovery();
});
console.log('Running on http://localhost:' + port);