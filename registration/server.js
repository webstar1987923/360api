var express 	= require('express');
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var jwt         = require('jsonwebtoken');
var config      = require('./config');
var request     = require('request-promise');
var q           = require('q');

var app         = express();
var port        = process.env.PORT || 8099;

var fs = require("fs");
var advisor_post_options = fs.readFileSync('advisor-post.json');
var client_post_options = fs.readFileSync('client-post.json');

var discoveryData = "";
var discoveryServiceData = "";

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// =================================================================
function initDiscovery(){
    request.get(config.discovery).then( function(data){
        discoveryData = JSON.parse(data)
    });
    request.get(config.discovery_services).then( function(data){
        discoveryServiceData = JSON.parse(data)
    });
}

function getDiscoveryLink(relation, data){
	var links = data["links"];
	var url = '';

	links.forEach(function (key, value){
		if(key.rel==relation){
			url = key.url_proxy;
			return;
		}
	});

	return url;
}
app.options('/', function(req, res) {
    res.json({"Allow": "POST,OPTIONS", "Content-Type": "application/json"});
});

app.options('/advisor', function(req, res) {
    res.json(JSON.parse(advisor_post_options));
});

app.options('/client', function(req, res) {
    res.json(JSON.parse(client_post_options));
});

app.post('/advisor', function(req, res) {
    var sessionPromise = registerSession(req);
    var companyPromise = sessionPromise.then(registerCompany.bind(null, req));

    q.all([sessionPromise, companyPromise]).then(registerAdvisor.bind(null, req, res)).
    catch(function (error) {
         console.log(error.toString('utf8'));
         return res.json({error:[{"message":error.message}]});
    }).
    done();
});

app.post('/client', function(req, res) {
    var sessionPromise = registerSession(req);

    q.all([sessionPromise]).then(registerClient.bind(null, req, res)).
    catch(function (error) {
         console.log(error.toString('utf8'));
         return res.json({error:[{"message":error.message}]});
    }).
    done();
});

function registerSession(req){
    var options = {
        method: 'POST',
        uri: getDiscoveryLink("session-registration", discoveryData),
        body: { email: req.body.email, password:req.body.password },
        headers: { "Content-Type": "application/json" },
        json: true
    };

    return request(options).then(function(data){
        if (data.success == false){
            throw new Error(data.message)
        }
        console.log("session created!");
        return data;
    });
}

function registerCompany(req, data){
    var options = {
        method: 'POST',
        uri: getDiscoveryLink("companies", discoveryServiceData),
        body: {
          name: req.body.company_name,
          document_upload: [{"name":"Dropbox", "isSelected":true},{"name":"Google Drive"},{"name":"Drive"},{"name":"Box"}],
          appointment_link: [{"name":"Calendly", "url":"https://calendly.com", "isSelected":true}, {"name":"ScheduleOnce",  "url":"http://www.scheduleonce.com"}],
          financial_link: [{"name":"Money",  "url":"https://www.emoneyadvisor.com", "isSelected":true}, {"name":"Right Capital", "url":"https://www.rightcapital.com"}, {"name":"Money Guide Pro", "url":"https://www.moneyguidepro.com/ifa"}],
          opening_hours: [{"hours":"Weekdays - 9.00 am to 5.00 pm | Off Saturday/Sunday", "isSelected":true}, {"hours":"Weekdays - 8.00 am to 4.00 pm | Off Saturday/Sunday"}, {"hours":"Weekdays - 9.00 am to 5.00 pm | Holiday - Sunday"}]
        },
        headers: {  "Content-Type": "application/json", "x-access-token": data.token},
        json: true
    };

    return request(options).
    then(function (body) {
        console.log("company created!");
        return body;
    });
}

function registerAdvisor(req, res, data){
    var short_id = data[1].short_id;
    var token = data[0].token;
    var session_id = data[0].user._id;
    var company_id = data[1]._id;
    var user = data[0].user;

    var options = {
        method: 'POST',
        uri: getDiscoveryLink("users", discoveryServiceData),
        body: {
              uid:session_id,
              first_name: req.body.first_name,
              last_name: req.body.last_name,
              email: req.body.email,
              roles: ["advisor", "subscriber"],
              groups: [short_id]
        },
        headers: {  "Content-Type": "application/json", "x-access-token": token},
        json: true
    };

    return request(options).
    then(function (body) {
        console.log("user created!");
        if (body.uid.length != 0){
            return res.json({
                success: true,
                token: token,
                links:[
                    {
                      "user": getDiscoveryLink("users", discoveryServiceData) + "/" + body._id,
                      "company": getDiscoveryLink("companies", discoveryServiceData) + "/" + company_id
                    }
                ]
            });
        }
    });
}

function registerClient(req, res, data){
    var token = data[0].token;
    var session_id = data[0].user._id;
    var user = data[0].user;
    var short_id = "TODO"

    var options = {
        method: 'POST',
        uri: getDiscoveryLink("users", discoveryServiceData),
        body: {
              uid:session_id,
              first_name: req.body.first_name,
              last_name: req.body.last_name,
              email: req.body.email,
              roles: ["client", "subscriber"],
              groups: [short_id]
        },
        headers: {  "Content-Type": "application/json", "x-access-token": token},
        json: true
    };

    return request(options).
    then(function (body) {
        console.log("user created!");
        if (body.uid.length != 0){
            return res.json({
                success: true,
                token: token,
                links:[
                    {
                      "user": getDiscoveryLink("users", discoveryServiceData) + "/" + body._id,
                    }
                ]
            });
        }
    });
}

// =================================================================

app.listen(port, function() {
  initDiscovery();
});
console.log('Running on http://localhost:' + port);