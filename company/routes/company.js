'use strict';

const User = require('../models/company');
var shortid = require('shortid');
var fs = require("fs");

var company_options = fs.readFileSync('company.json');

module.exports = server => {

   /* server.options('/', (req, res, next) => {
        res.json({"Allow": "GET, PUT, POST, OPTIONS", "Content-Type": "application/json"});
    });
*/
    server.get('/companiesoptions', (req, res, next) => {
        res.json(JSON.parse(company_options));
    });

    /**
     * Handle GET requests on companies
     * Return a list of all the companies to the client
     */
    server.get('/companies', (req, res, next) => {
        if(req.query.short_id){
            User.findOne({short_id:req.query.short_id},(err, companies) => {
                res.send(companies);
                next();
            });
        }else{
            User.find((err, companies) => {
                res.send(companies);
                next();
            });
        }
    });
    
    
    /**
     * Handle GET requests on specific company
     * Return a description of the company to the client
     */
    server.get('/companies/:id', (req, res, next) => {
        User.findOne({ _id:req.params.id }, (err, company) => {
            if (err) {
                res.send({err});
            }
            if(company != null){
                res.send(company);
                next();
            }
        });
    });
    
    /**
     * Handle POST requests on companies
     * Create a new company in database
     */
    server.post('/companies', (req, res, next) => {
        let company = new User({
            name: req.body.company_name,
        	logo_link: req.body.logolink,
        	theme: req.body.theme,
        	address_one: req.body.address_one,
        	address_two: req.body.address_two,
        	city: req.body.city,
        	zip: req.body.zip,
        	state: req.body.state,
        	email: req.body.email,
        	phone: req.body.phone,
        	document_upload: req.body.document_upload,
        	appointment_link: req.body.appointment_link,
        	financial_link: req.body.financial_link,
        	opening_hours: req.body.opening_hours,
            short_id: shortid.generate()
        });
        for (let prop in req.body)
            company[prop] = req.body[prop];
        company.save().then(company => {
            res.send(company);
            next();
        });
    });
    
    /**
     * Handle PUT requests on specific company
     * Update an existing company in database
     */
    server.put('/companies/:id', (req, res, next) => {
        User.findOne({ _id:req.params.id }, (err, company) => {
            for (let prop in req.body)
                company[prop] = req.body[prop];
            company.save().then(company => {
                res.send(company);
                next();
            });
        });
    });
    
    /**
     * Handle DELETE requests on specific company
     * Delete an existing company in database
     */
    server.del('/companies/:id', (req, res, next) => {
        User.remove({ _id: req.params.id }, err => {
            res.send({ok: 'ok'});
            next();
        });
    });
    
}