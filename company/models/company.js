'use strict';

let mongoose = require('mongoose');

/* The schema describing the collection in mongo database */
let schema = new mongoose.Schema({
	name: String,
	logo_link: String,
	theme: String,
	address_one: String,
	address_two: String,
	city: String,
	zip: String,
	state: String,
	email: String,
	phone: String,
	document_upload: [{name: String, isSelected: Boolean}],
	appointment_link: [{name: String, url: String, isSelected: Boolean}],
	financial_link: [{name: String, url: String, isSelected: Boolean}],
	opening_hours: [{hours: String, isSelected: Boolean}],
	short_id: String
});

/* We export the model to use it in user.js routes file */
module.exports = mongoose.model('company', schema);