'use strict';

const express = require('express');

// Constants
const PORT = 8089;

// App
const app = express();
app.use(express.static(__dirname + '/public'));

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);