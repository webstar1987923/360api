'use strict';

let mongoose = require('mongoose');

let schema = new mongoose.Schema({
	email_to: String,
	email_from: String,
	short_id: String,
	expiration_date: Number,
	pass_code: String
});

module.exports = mongoose.model('invitation', schema);