'use strict'
const jwt = require('jsonwebtoken')

module.exports = function jwtAuthMiddleware (opts) {
    const options = Object.assign({}, opts)
    options.whitelist = opts.whitelist || false
    options.blacklist = opts.blacklist || false
    options.handler = opts.handler || function (req, res, next) {
        return res.redirect('/login', next)
    }

    const middleware = (req, res, next) => {
        // if whitelist pass through anyhow
        if (options.whitelist && options.whitelist.indexOf(req.getPath()) !== -1) {
            next()
        }

        if (options.blacklist && options.blacklist.indexOf(req.getPath()) !== -1) {
            return options.handler(req, res, next)
        }

        // if request header, verify and decode it
        // if not valid anymore, push requestee to login again
        if (req.headers['x-access-token']) {

           let credentials = req.headers['x-access-token']
           jwt.verify(credentials, options.secret, function (err, decoded) {
                if (err) {
                    return options.handler(req, res, next)
                }
                // attach to req object
                req['decoded'] = decoded
                return next() // suceess
           })
        } else {
            return options.handler(req, res, next)
        }
    }

    return middleware
}