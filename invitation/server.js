'use strict';

let restify  = require('restify'),
    mongoose = require('mongoose'),
    jwtAuth = require('./auth/custom-jwt');
    
mongoose.connect('mongodb://mongo/invitations');

let server = restify.createServer();

let handler = function (req, res, next) {
    return res.redirect('/api/sessions/login', next)
}

let options = {
    secret: 'applesandoranges',
    blacklist: [], // blacklist routes like '/noaccess', will always return handler
    whitelist: [], // whitelist routes like '/immediate', no auth needed
    handler: handler // default is redirect as above. Will send a 302 and the login route
}

server.use(restify.bodyParser({ mapParams: true }));
server.use(jwtAuth(options));


require('./routes/index')(server);

server.listen(8130, function() {
  console.log('%s listening at %s', server.name, server.url);
});