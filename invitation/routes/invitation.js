'use strict';

const User = require('../models/invitation');
const jwt    = require('jsonwebtoken');
var shortid = require('shortid');

module.exports = server => {
    
   server.get('/invitations', (req, res, next) => {
        User.find((err, companies) => {
            res.send(companies);
            next();
        });

   });

    /**
     * Handle GET requests on specific invitation
     * Return a description of the invitation to the client
     */
    server.get('/invitations/:id', (req, res, next) => {
        User.findOne({ _id:req.params.id }, (err, invitation) => {
            res.send(invitation);
            next();
        });
    });


    
    /**
     * Handle POST requests on invitations
     * Create a new invitation in database
     */
    server.post('/invitations', (req, res, next) => {
        var token = req.headers["x-access-token"];
        var decoded = jwt.decode(token);
        var short_id = "";
        User.findOne({email:decoded._doc["email"]}, (err, user) => {
            short_id = user.groups[0];
        });
        let invitation = new User({
            email_to: req.body.email_to,
            email_from: decoded._doc["email"],
            short_id: req.body.short_id,
            expiration_date: 86400,
            pass_code: req.body.shortid.generate()
        });
        for (let prop in req.body)
            invitation[prop] = req.body[prop];
        invitation.save().then(invitation => {
            res.send(invitation);
            next();
        });
    });
    
    /**
     * Handle PUT requests on specific invitation
     * Update an existing invitation in database
     */
    server.put('/invitations/:id', (req, res, next) => {
        User.findOne({ _id:req.params.id }, (err, invitation) => {
            for (let prop in req.body)
                invitation[prop] = req.body[prop];
            invitation.save().then(invitation => {
                res.send(invitation);
                next();
            });
        });
    });
    
    /**
     * Handle DELETE requests on specific invitation
     * Delete an existing invitation in database
     */
    server.del('/invitations/:id', (req, res, next) => {
        User.remove({ _id: req.params.id }, err => {
            res.send({ok: 'ok'});
            next();
        });
    });
    
}