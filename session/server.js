const express 	= require('express');
const app         = express();
const bodyParser  = require('body-parser');
const morgan      = require('morgan');
const mongoose    = require('mongoose');
const jwt    = require('jsonwebtoken');
const config = require('./config');
const Usermodel = require('./app/models/user')
const port = process.env.PORT || 8097;
mongoose.connect(config.database);
const User = mongoose.model('User');
app.set('superSecret', config.secret);
var Client = require('node-rest-client').Client;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('dev'));

function getDiscoveryLink(relation, data){
	var links = data["links"];
	var url = '';

	links.forEach(function (key, value){
		if(key.rel==relation){
			url = key.url_proxy;
			return;
		}
	});

	return url;
}

function getDiscoveryData(user, callback){
    var client = new Client();
	client.registerMethod("discovery", config.discovery, "GET");
	client.methods.discovery(function (discovery_data, response) {
	    var clientUser = new Client();
    	clientUser.registerMethod("user_profile", getDiscoveryLink("user-profile", discovery_data), "GET");
    	clientUser.methods.user_profile(function (discovery_data, response) {
    	    callback(discovery_data, response);
    	});
	});
}
// =================================================================

app.post('/registration', function(req, res) {
	const user = new User(req.body);

	user.save(function(err) {
		console.log('User saved successfully');

		var token = jwt.sign(user, app.get('superSecret'), {
			expiresIn: 86400 // expires in 24 hours
		});

        if (err){
            res.json({
                success: false,
                message: "user already exists!"
            });
        }else{
            res.json({
                success: true,
                user:user,
                token: token
            });
        }
	});
});


var apiRoutes = express.Router();

// =================================================================

apiRoutes.post('/creation', function(req, res) {
    const user = new User(req.body);
	User.findOne({
		email: req.body.email
	}, function(err, user) {

		if (err) throw err;

		if (!user) {
			res.json({ success: false, message: 'Authentication failed' });
		} else if (user) {
			if (!user.authenticate(req.body.password)) {
				res.json({ success: false, message: 'Authentication failed' });
			} else {
				var token = jwt.sign(user, app.get('superSecret'), {
					expiresIn: 86400 // expires in 24 hours
				});

                res.json({
                    success: true,
                    token: token,

                });
			}
		}

	});
});

// =================================================================

apiRoutes.use(function(req, res, next) {

	var token = req.body.token || req.param('token') || req.headers['x-access-token'];

	if (token) {
		jwt.verify(token, app.get('superSecret'), function(err, decoded) {			
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });		
			} else {
				req.decoded = decoded;
				next();
			}
		});

	} else {
		return res.status(403).send({
			success: false, 
			message: 'No token provided.'
		});
		
	}
	
});

// =================================================================

apiRoutes.get('/check', function(req, res) {
	res.json(req.decoded);
});

app.use('/', apiRoutes);


app.listen(port);
